<?php
/*
Plugin Name: Powerhut Then and Now
Description: Plugin description
Plugin URI: https://powerhut.net/
Author: Graham Washbrook
Author URI: https://powerhut.net
Text Domain: plugin-text-domain
Domain Path: /languages
Version: 0.0.8
*/

//* Search and Replace Strings
// namespace = ns_
// defined namespace path = NS_PATH ~ e.g. PHUT_THEN_NOW_PATH
// custom post type name = cpt_name ~ e.g. phut_testimonial
// image size(s) = cpt-name-archive ~ e.g. phut-testimonial-archive
// custom post type slug = cpt-slug ~ e.g. 
// custom post type plural label = CPT_PLURAL_LABEL ~ e.g. Testimonials
// custom post type singular label = CPT_SINGLE_LABEL ~ e.g. Testimonial
// custom tag name = tag_name ~ e.g. phut_tantag
// custom tag plural label (lower case) = TAG_PLURAL_LC_LABEL ~ e.g. timelines
// custom tag plural label = TAG_PLURAL_LABEL ~ e.g. Timelines
// custom tag singular label = TAG_SINGLE_LABEL ~ e.g. Timeline

//* Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//* Define plugin path
define( 'PHUT_THEN_NOW_PATH', plugin_dir_path( __FILE__ ) );



//* Includes and requires
require( PHUT_THEN_NOW_PATH . 'config.php');

//* On plugin activation
function phut_then_now_activation() {
	if ( ! current_user_can( 'activate_plugins' ) )  return;
	phut_then_now_init();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'phut_then_now_activation' );

//* On plugin deactivation
function phut_then_now_deactivation() {
	flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'phut_then_now_deactivation' );

//* On plugin uninstall
function phut_then_now_uninstall() {
	/*
	// e.g. delete options
	$option_name = 'phut_then_now_option';
 	delete_option($option_name);
 
	// for site options in Multisite
	delete_site_option($option_name);
 
	// eg. drop a custom database table
	global $wpdb;
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}mytable");
	*/	
}
register_uninstall_hook( __FILE__, 'phut_then_now_uninstall');

//* Init
function phut_then_now_init() {

	// Register taxonomies
	phut_then_now_register_taxonomies();

	// Register post types
	phut_then_now_register_cpt();

	// Better be safe than sorry when registering custom taxonomies for custom post types.
	register_taxonomy_for_object_type( 'phut_tantag', 'phut_thenandnow' );

	// Change enter title here text
	add_filter( 'enter_title_here', 'phut_then_now_enter_title_here' );

	// Change the post updated messages
	add_filter( 'post_updated_messages', 'phut_then_now_updated_messages' );

	// Change the bulk post updated messages
	add_filter( 'bulk_post_updated_messages', 'phut_then_now_bulk_updated_messages' );

	// Add image size
	add_image_size( 'phut-thenandnow-archive', 370, 370, array( 'left', 'top' ) );
	
} //fn
add_action( 'init', 'phut_then_now_init', 0 );

//* Register taxonomies
function phut_then_now_register_taxonomies() {

	// TAG
	
	$labels = array(

		'name'          => _x( 'Then and Now Tags', 'taxonomy general name' ),
		'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
		'menu_name'     => _x( 'Tags', 'taxonomy general name' ),
		'all_items'     => __( 'All Tags', 'plugin-text-domain' ),
		'edit_item'     => __( 'Edit Tag', 'plugin-text-domain' ),
		'view_item'     => __( 'View Tag', 'plugin-text-domain' ),
		'update_item'   => __( 'Update Tag', 'plugin-text-domain' ),
		'add_new_item'  => __( 'Add New Tag', 'plugin-text-domain' ),
		'new_item_name' => __( 'New Tag', 'plugin-text-domain' ),
		'search_items'  => __( 'Search Tags', 'plugin-text-domain' ),
		'popular_items' => __( 'Popular Tags', 'plugin-text-domain' ),
		'not_found'     => __( 'No tags found', 'plugin-text-domain' ),

		// FOR NON HIERARCHY ( tags )
		'separate_items_with_commas' => __( 'Separate tags with commas', 'plugin-text-domain' ),
		'add_or_remove_items'        => __( 'Add or remove tags', 'plugin-text-domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used tags', 'plugin-text-domain' ),

		// FOR HIERARCHY ( categories )
		// 'parent_item'       => __( 'Parent Tag', 'plugin-text-domain' ),
		// 'parent_item_colon' => __( 'Parent Tag:', 'plugin-text-domain' ), // Same as parent_item, but with a colon :

	);
	
	$rewrite = array(
		'slug'         => 'then-now-tag',  // Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
		'with_front'   => true,         // allowing permalinks to be prepended with front base - defaults to true
		'hierarchical' => false,         // Boolean. Allow hierarchical urls (implemented in Version 3.1)  - default false	
	);
	
	/*
	$capabilities = array(
		'manage_terms' => 'manage_months',
		'edit_terms'   => 'edit_months',
		'delete_terms' => 'delete_months',
		'assign_terms' => 'assign_months'
	);
	*/
	
	$args = array(

		'labels'             => $labels,
		'public'             => true, // (boolean) optional ~ default true
			'publicly_queryable' => true,  // (boolean) optional ~ defaults to value of 'public'
			'show_in_nav_menus'  => true, // (boolean) optional ~ defaults to value of 'public'
			'show_ui'            => true,  // (boolean) optional ~ defaults to value of 'public'

		'show_in_menu'       => true, // (boolean) optional - here to show taxonomy in admin menu. 'show_ui' must be true ~ defaults to 'show_ui'
		'show_tagcloud'      => true, // (boolean) optional - Whether to allow the Tag Cloud widget to use this taxonomy ~ defaults to 'show_ui'
		'show_in_quick_edit' => true, // (boolean) optional ~ defaults to 'show_ui'
		
		

		
		
		// 'meta_box_cb'       => '', // (callback) optional ~ defaults to post_tags_meta_box() or post_categories_meta_box(). No metabox is shown if set to false
		'show_admin_column' => true, // (boolean) optional ~ default false
		'description'       => '', // (string) optional - Include a description of the taxonomy ~ default ''
		'hierarchical'      => false,  // (boolean) optional ~ default false
		'update_count_callback' => '_update_post_term_count', // To ensure taxonomy acts like a tag, set to '_update_post_term_count'
		'query_var'         => false, // false || string // default taxonomy name
		// 'capabilities'      => $capabilities,
		// 'sort' => true, // (boolean) optional - Whether should remember order that terms are added to objects ~ default None
		
		'rewrite'           => $rewrite,
		'delete_with_user'  => false,
		
		// 'show_in_rest'       => true, // (boolean) optional ~ default false
    	// 'rest_base'          => '*******', // (string) (optional) To change the base url of REST API route
		// 'rest_controller_class' =>


	);
	
	register_taxonomy( 'phut_tantag', array( 'phut_thenandnow' ), $args );
	// register_taxonomy( 'phut_month', array( 'phut_ingredient','phut_recipe' ), $args );


	// TODO Comment out when in production
	flush_rewrite_rules( false );

} // fn

//* Register post types
function phut_then_now_register_cpt() {

	$supports = array (
		'title',
		'editor',
		'excerpt',
		'author',
		'thumbnail',
		'custom_fields',
		// 'comments',
		// 'trackbacks',
		// 'revisions',
		// 'page-attributes',                  // menu order ( hierarchical post types only )
		// 'post-formats',
		// 'genesis_seo',
		'genesis-layouts',
		// 'genesis-simple-sidebars',
		'genesis-scripts',
		'genesis-cpt-archives-settings',
		// 'genesis-entry-meta-after-content',
		// 'autodescription-meta',             // The SEO Framework
		
	);

	$labels = array (
		'name'                  => 'Then and Now', // Seen in : Breadcrumbs, All Items list
		'singular_name'         => 'Then and Now',
		'menu_name'             => 'Then and Now', // Default - same as 'name'
		'name_admin_bar'        => 'Then and Now', // For use in New in Admin menu bar. Default is the same as `singular_name`.
		'all_items'             => 'All Then and Now', // String for the submenu
		'add_new'               => 'Add New',
	 	'add_new_item'          => __( 'Add a new Then and Now', 'plugin-text-domain' ),
		'edit_item'             => __( 'Edit Then and Now', 'plugin-text-domain' ),
		'new_item'              => __( 'New Then and Now', 'plugin-text-domain' ),
		'view_item'             => __( 'View Then and Now', 'plugin-text-domain' ),
		'search_items'          => __( 'Search Then and Now', 'plugin-text-domain' ),
		'not_found'             => __( 'No Then and Nows found', 'plugin-text-domain' ),
		'not_found_in_trash'    => __( 'No Then and Nows found in Bin', 'plugin-text-domain' ),
		// 'parent_item_colon'     => __( 'Parent Then and Now', 'plugin-text-domain' ), // only used in hierarchical post types
		'archives'              => __( 'Then and Now', 'plugin-text-domain' ), // for use with archives in nav menus. Default is Post Archives/Page Archives.
		'insert_into_item'      => __( 'Insert into Then and Now', 'plugin-text-domain' ), // for the media frame button. Default is Insert into post/Insert into page.
		'uploaded_to_this_item' => __( 'Uploaded to this Then and Now', 'plugin-text-domain' ), // for the media frame filter. Default is Uploaded to this post/Uploaded to this page.
		'attributes'            => __( 'Then and Now Attributes', 'plugin-text-domain' ), // for the attributes meta box. Default is 'Post Attributes' / 'Page Attributes'.
		'featured_image'        => __( 'Featured Image', 'plugin-text-domain' ), // Default is Featured Image.
		'set_featured_image'    => __( 'Set featured image', 'plugin-text-domain' ), // Default is Set featured image.
		'remove_featured_image' => __( 'Remove featured image', 'plugin-text-domain' ), // Default is Remove featured image.
		'use_featured_image'    => __( 'Use as featured image', 'plugin-text-domain' ), // Default is Use as featured image. 
		
	);
	
	$rewrite = array (
		'slug'       => 'then-now',
		'with_front' => false, // eg: if permalink structure is /blog/, then links will be: false->/news/, true->/blog/news/). Defaults to true
		'feeds'      => false, // Defaults to has_archive value
		'pages'      => true, // Should the permalink structure provide for pagination. Defaults to true
	);
	
	$taxonomies = array (
		'phut_tantag',
	);
	
	$args = array (
		'label'                => 'Then and Now', // Shown in breadcrumbs, archive title, admin list table, menu items
		'labels'               => $labels,
		'description'          => __( 'CPT Description goes here', 'plugin-text-domain' ), // (string) (optional) A short descriptive summary of what the post type is. May be visible, e.g. in menus
		'public'               => true,  // true || false - optional : Needs to be true for Yoast
			'exclude_from_search' => true, // needs to be false if want to see cpts on cpt-archive page. If need to exclude from search, then filter the search query
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => true, // Preferably true if has_archive is true
			'show_ui'             => true,
		
		'show_in_menu'         => true,
		'show_in_admin_bar'    => true,
		// 'menu_position'        => null, // Defaults to below Comments
		'menu_icon'            => 'dashicons-image-flip-horizontal',
		'capability_type'      => 'post', // or 'phut_thenandnow' referenced
		'map_meta_cap'         => true,
		'hierarchical'         => false,
		'supports'             => $supports,
		
		// Called when setting up the meta boxes for the edit form.
		// Takes one argument $post, which contains the WP_Post object for the currently edited post.
		// Do remove_meta_box() and add_meta_box() calls in the callback.
		'register_meta_box_cb' => 'phut_then_now_meta_box_cb',
		
		// 'taxonomies' => $taxonomies,
		'has_archive'          => false,
		'rewrite'              => $rewrite,		
		'query_var'            => true, // defaults to true (custom post type name)
		'can_export'           => true, // default true
		
		// 'show_in_rest'         => true, // default false
		// 'rest_base'            => 'restbase', // The base slug that this post type will use when accessed using the REST API.
		
		/* Visibility / Archive Overide */
		'exclude_from_search' => false, // needs to be false if want to see cpts on cpt-archive page. If need to exclude from search, then filter the search query
		'publicly_queryable'  => true,		
		'has_archive'         => true,
		
		
		/* public/show_ui/show_in_menu/has_archive all need to be true for Genesis CPT archives */
		'public'       => true,
		'show_ui'      => true,
		'show_in_menu' => true,
		'has_archive'  => true,
		
		
	);
	
	register_post_type( 'phut_thenandnow', $args );
	
	// Comment or remove following flush rewrite rules when not in development !!!
	flush_rewrite_rules();

}

//* Change enter title here text
function phut_then_now_enter_title_here( $title ) {

	$screen = get_current_screen();
	if( $screen->post_type == 'phut_thenandnow' )
		$title = 'Enter title here';

	return $title;
}

//* Change the post updated messages
function phut_then_now_updated_messages( $messages ) {

	global $post;

	// Add messages to $messages array
	$messages['phut_thenandnow'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( 'Then and Now updated. <a href="%s">View Then and Now</a>', esc_url( get_permalink($post->ID) ) ),
		2  => 'Custom field updated.',
		3  => 'Custom field deleted.',
		4  => 'Then and Now updated.',
		5  => isset( $_GET['revision']) ? sprintf( 'Then and Now restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( 'Then and Now published. <a href="%s">View Then and Now</a>', esc_url( get_permalink( $post->ID ) ) ),
		7  => 'Then and Now saved.',
		8  => sprintf( 'Then and Now submitted. <a target="_blank" href="%s">Preview Then and Now</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
		9  => sprintf( 'Then and Now publishing scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Then and Now</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post->ID ) ) ),
		10 => sprintf( 'Then and Now draft updated. <a target="_blank" href="%s">Preview Then and Now</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
	);

	return $messages;
}

//* Change the bulk post updated messages
function phut_then_now_bulk_updated_messages( $bulk_messages ) {

	global $bulk_counts;
	
	// Add messages to $bulk_messages array
	$bulk_messages['phut_thenandnow'] = array(
		'updated'   => _n( '%s Then and Now updated.', '%s Then and Nows updated.', $bulk_counts['updated'] ),
		'locked'    => _n( '%s Then and Now not updated, somebody is editing it.', '%s Then and Nows not updated, somebody is editing them.', $bulk_counts['locked'] ),
		'deleted'   => _n( '%s Then and Now permanently deleted.', '%s Then and Nows permanently deleted.', $bulk_counts['deleted'] ),
		'trashed'   => _n( '%s Then and Now moved to Bin.', '%s Then and Nows moved to Bin.', $bulk_counts['trashed'] ),	
		'untrashed' => _n( '%s Then and Now restored from Bin.', '%s Then and Nows restored from the Bin.', $bulk_counts['untrashed'] ),
	);

	return $bulk_messages;

}

//*
function phut_then_now_meta_box_cb() {

	//

}


/*
add_action( 'genesis_entry_footer', 'add_tax_terms' );

function add_tax_terms() {

	if ( is_singular( 'phut_thenandnow' ) ) {
	
		$terms = get_the_term_list( get_the_ID(), 'phut_tantag', 'Taxonomy Terms : ', ', ' );

		if ( is_wp_error( $terms ) )
		return '';

		if ( empty( $terms ) )
		return '';

    	echo $terms;
	}
}
*/



/*
 * Use custom templates
 *
**/

add_filter('single_template', 'phut_then_now_custom_templates');
add_filter('taxonomy_template', 'phut_then_now_custom_templates');
add_filter('archive_template', 'phut_then_now_custom_templates');

function phut_then_now_custom_templates( $template ) {


	if ( is_singular('phut_thenandnow') )
		return PHUT_THEN_NOW_PATH . 'templates/single-phut_thenandnow.php';


	if ( is_post_type_archive('phut_thenandnow') || is_tax('phut_tantag') )
		return PHUT_THEN_NOW_PATH . 'templates/archive-phut_thenandnow.php';

	return $template;
}

// Conditionally exclude images from Jetpack Photon
function phut_then_now_no_photon() {
  if ( is_singular( 'phut_thenandnow' ) ) {
    add_filter( 'jetpack_photon_skip_image', '__return_true');
  }
}
add_action('wp', 'phut_then_now_no_photon');
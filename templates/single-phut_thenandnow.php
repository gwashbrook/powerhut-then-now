<?php

add_action('wp_head','phut_then_now_wp_head');
function phut_then_now_wp_head () {
?>
<style type="text/css">
.acf-map {
	width: 100%;
	height: 400px;
	border: #ccc solid 1px;
	margin-bottom: 1.5em;
}
.acf-map img {
   max-width: inherit !important;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBMQ581oYqnruaLVYsU7XhPenwhHT4RDU"></script>
<script type="text/javascript">
(function($) {
function new_map( $el ) {
	var $markers = $el.find('.marker');
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map( $el[0], args);
	map.markers = [];
	$markers.each(function(){
    	add_marker( $(this), map );
	});
	center_map( map );
	return map;
}
function add_marker( $marker, map ) {
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});
	map.markers.push( marker );
	if( $marker.html() )
	{
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open( map, marker );
		});
	}
}
function center_map( map ) {
	var bounds = new google.maps.LatLngBounds();
	$.each( map.markers, function( i, marker ){
		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
		bounds.extend( latlng );
	});
	if( map.markers.length == 1 )
	{
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		map.fitBounds( bounds );
	}
}
var map = null;
$(document).ready(function(){
	$('.acf-map').each(function(){
		map = new_map( $(this) );
	});
});
})(jQuery);
</script>


<?php
}


// Add Map Area to top of page

function phut_then_now_map() {
	?>
	<div class="acf-map" style="height:400px;background:pink;">Google map here</div>
	<?php
}
// add_action( 'genesis_before_loop','phut_then_now_map', 999);



add_action( 'genesis_after_entry_content','phut_then_now_single_map', 9);
function phut_then_now_single_map () {
	$location = get_field('location');
	if( !empty($location) ):
?>
<a class="anchor" name="thennowmap"></a>
<div class="acf-map"><div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"><h4>Title</h4><p>Some dynamic text</p></div></div>
<?php endif; ?>


<?php
}











genesis();
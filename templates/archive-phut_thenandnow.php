<?php


/**
 * Javascript for Load More
 *
 */

function be_load_more_js() {
	
	global $wp_query;
	
	$args = array(
		'nonce' => wp_create_nonce( 'be-load-more-nonce' ),
		'url'   => admin_url( 'admin-ajax.php' ),
		'query' => $wp_query->query,
		'maxpage' => $wp_query->max_num_pages,
	);
	
	
	wp_enqueue_script( 'be-load-more', get_stylesheet_directory_uri() . '/js/load-more.js', array( 'jquery' ), '1.0', true );
	wp_localize_script( 'be-load-more', 'beloadmore', $args );

}
add_action( 'wp_enqueue_scripts', 'be_load_more_js' );



//* Enqueue and initialize jQuery Masonry script
function phut_then_now_masonry_init() {
	wp_enqueue_script( 'masonry-init', plugins_url() . '/powerhut-then-now/public/js/masonry-init.js' , array( 'jquery-masonry' ), '1.0', true );
 }
add_action( 'wp_enqueue_scripts', 'phut_then_now_masonry_init' );




// Add Map Area to before the loop
function phut_then_now_map() {
	?>
	<div class="acf-map" style="height:400px;background:pink;margin-bottom: 16px">Google map of "Then and Now" locations here</div>
	<?php
}
// add_action( 'genesis_before_loop','phut_then_now_map', 999);


// Wrap loop in masonry grid
function phut_open_grid(){
	echo '<div class="grid"><div class="column-sizer"></div><div class="gutter-sizer"></div>';	
}
add_action('genesis_before_while','phut_open_grid');


function phut_close_grid(){
	echo '</div>';	
}
add_action('genesis_after_endwhile','phut_close_grid',9);







// Set schema ??


// Relocate featured image
remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
add_action( 'genesis_entry_header', 'genesis_do_post_image', 3 );


add_action( 'genesis_entry_header', function(){ echo '<div class="entry-wrap">'; }, 4 ); // Might clash with post format image
add_action( 'genesis_after_entry_content', function(){ echo '</div>'; }, 20 );



// Remove Archive Pagination - when "Load More" is working
// remove_action( 'genesis_after_endwhile', 'genesis_posts_nav' );

// Remove entry meta from entry header
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'phut_then_now_archive_loop');

function phut_then_now_archive_loop(){

	if ( have_posts() ) :

		do_action( 'genesis_before_while' );
		
		while ( have_posts() ) : the_post();

	
			do_action( 'genesis_before_entry' );

			
			printf( '<article %s>', genesis_attr( 'entry' ) );
			
			do_action('genesis_entry_header');
			
			do_action('genesis_before_entry_content');
			printf( '<div %s>',genesis_attr('entry-content'));
			do_action('genesis_entry_content');
			echo '</div>';
			do_action('genesis_after_entry_content');
			do_action('genesis_entry_footer');
			echo '</article>';
			
			




			do_action( 'genesis_after_entry' );

		endwhile; //* end of all posts



		do_action( 'genesis_after_endwhile' );
		
	else : // if no posts exist
		do_action( 'genesis_loop_else' );
	endif; // end loop
	
} // phut_then_now_archive_loop 



// add_action( 'genesis_after_entry_content','');

add_action( 'genesis_entry_footer', 'phut_then_now_footer');

function phut_then_now_footer() {
	
	echo '<p class="entry-meta">';
	echo get_the_term_list( get_the_ID(), 'phut_tantag', '<span class="entry-tags">', ' ', '</span>' );
	echo '</p>';

}

genesis();
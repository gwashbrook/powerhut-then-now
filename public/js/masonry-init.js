jQuery(function($) {
	"use strict";
	var  $grid = $('.grid');
	$grid.imagesLoaded( function(){
		$grid.masonry({
			columnWidth: '.column-sizer',
			gutter: '.gutter-sizer',
			itemSelector: '.type-phut_thenandnow',
			isAnimated: true,
			percentPosition: true
		});
	});
});